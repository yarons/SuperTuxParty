# Translations template for Super Tux Party.
# Copyright (C) 2019 Super Tux Party Contributors
# This file is distributed under the same license as the Super Tux Party project.
#
msgid ""
msgstr ""
"Project-Id-Version: 0.7\n"
"PO-Revision-Date: 2020-04-30 12:11+0000\n"
"Last-Translator: Florian Kothmeier <floriankothmeier@web.de>\n"
"Language-Team: Italian <https://hosted.weblate.org/projects/super-tux-party/"
"minigameshurdle/it/>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.0.2\n"

msgid "HURDLE_PLAYER_WINS_MSG"
msgstr "{player} ha vinto!"
